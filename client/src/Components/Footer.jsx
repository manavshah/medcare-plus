import react from 'react';

const Footer = () => {
    return (
        <>
            <footer>
                <div className="footer-wrapper" style={{background: 'rgba(33, 132, 187, 1)', padding: '20px', color: '#fff'}}>
                    <p className="text-center pt-3">Developed with ❤️@2021 copyright</p>
                </div>
            </footer>
        </>
    );
};

export default Footer;